"use strict";
const DbService = require("moleculer-db");
const SqlAdapter = require("moleculer-db-adapter-sequelize");
const Sequelize = require("sequelize");
const conexion = {
    "database": "demo_uas",
    "username": "root",
    "pwd": "",
    "config": {
        "dialect": "mysql",
        "host": "localhost"
    }
};
module.exports = {
	name: "RegistroACurso",

	mixins: [DbService],

	adapter: new SqlAdapter(conexion.database, conexion.username, conexion.pwd, conexion.config),
	model: {
		name: "RegistroACurso",
        define: {
			nombre: Sequelize.STRING,
			apellido: Sequelize.STRING,
			curso: Sequelize.STRING,
			horario: Sequelize.STRING,
			correo: Sequelize.STRING,
		
            // content: Sequelize.TEXT,
            // votes: Sequelize.INTEGER,
            // author: Sequelize.INTEGER,
            // status: Sequelize.BOOLEAN
        },
        options: {
            // Options from http://docs.sequelizejs.com/manual/tutorial/models-definition.html
        }
    },
	settings: {
		onError(req, res, err) {
            res.setHeader("Content-Type", "application/json");
            res.writeHead(err.code || 500);
            res.end(JSON.stringify({
                success: false,
                message: err.message
            }));
        }	

	},


	/**
	 * Service dependencies
	 */
	//dependencies: [],	

	/**
	 * Actions
	 */
	actions: {

		//funciones GET
		hello() {
			var insert = {
				nombre:"ruben dario",
				especialidad:"zamorano cervantes",
			
			}

			var update = {
				nombre:"ruben dario",
				especialidad:"zamorano cervantes",
			
			}
			// //use query
			// return this.adapter.db.query("select * from alumnos").then(result => {
			// 	return result[0];
			// });

			// //insert from model
			// return this.model.create(insert).then(result => {
			// 	return result[0];
			// });

			// //insert
			// return this.adapter.insert(insert).then(result => {
			// 	return result;
			// });

			// //find
			// return this.adapter.find({edad:"25"}).then(result => {
			// 	return result;
			// });

			// //findById
			// return this.adapter.findById(1).then(result => {
			// 	return result;
			// });

			// //updateById
			// return this.adapter.updateById(4,{$set:update}).then(result => {
			// 	return result;
			// });

			// //updateMany
			// return this.adapter.updateMany({edad:"26"},update).then(result => {
			// 	return result;
			// });

			// //removeById
			// return this.adapter.removeById(1).then(result => {
			// 	return result;
			// });

			// //removeMany
			// return this.adapter.removeMany({nombre:"ruben2"}).then(result => {
			// 	return result;
			// });

			// //list
			// return this.model.findAll({limit:10,offset:0,where:{edad:"25"}}).then(result => {
			// 	return result;
			// });

			// //count
			// return this.adapter.count({}).then(result => {
			// 	return result;
			// });
		},


		//funciones POST

		addPreinscripcion: {
			handler(ctx) {
				// data:ctx.params
				//insert
				var insert = ctx.params.RegistroACurso;
				return this.adapter.insert(insert).then(result => {
					return result;
				});
			}
		},
	
	},

	/**
	 * Events
	 */
	events: {

	},

	/**
	 * Methods
	 */
	methods: {

	},

	/**
	 * Service created lifecycle event handler
	 */
	created() {

	},

	/**
	 * Service started lifecycle event handler
	 */
	started() {

	},

	/**
	 * Service stopped lifecycle event handler
	 */
	stopped() {

	}	
};