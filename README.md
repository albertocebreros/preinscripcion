# demo_uas

## Build Setup

``` bash
# Install dependencies
npm install

# Start developing with REPL
npm run dev

# Start production
npm start
```

## Run in Docker

**Build Docker image**
```bash
$ docker build -t demo_uas .
```

**Start container**
```bash
$ docker run -d demo_uas
```
